<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Mostra uma listagem de clientes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::all();
    }


    /**
     * Armazene um cliente criado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'razao' => 'required',
            'fantasia' => 'required',
            'cnpj' => 'nullable',
            'email' => 'required',
            'efinanceiro' => 'required',
            'cep' => 'required',
            'endereco' => 'nullable',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'complemento' => 'required',
            'celular' => 'required',
            'telefone' => 'required',
        ]);

        return Client::create($request->all());
    }

    /**
     * Mostrar cliente específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Client::find($id);
    }


    /**
     * Modificar cliente.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $client->update($request->all());
        return $client;
    }

    /**
     * Remover cliente específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Client::destroy($id);
    }

    /**
     * Pesquisar cliente pelo nome.
     *
     * @param  str  $razao
     * @return \Illuminate\Http\Response
     */
    public function serach($razao)
    {
        return Client::where('razao', 'like', '%'.$razao.'%')->get();
    }
}
