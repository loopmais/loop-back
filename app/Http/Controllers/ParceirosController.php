<?php

namespace App\Http\Controllers;

use App\Models\Parceiro;
use Illuminate\Http\Request;

class ParceirosController extends Controller
{
    /**
     * Mostra a listagem de parceiros.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Parceiro::all();
    }


    /**
     * Armazene um parceiro criado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'razao' => 'required',
            'fantasia' => 'required',
            'cnpj' => 'required',
            'cep' => 'required',
            'endereco' => 'required',
            'numero' => 'nullable',
            'complemento' => 'nullable',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'email' => 'required',
            'efinanceiro' => 'nullable',
            'celular' => 'required',
            'telefone' => 'nullable',
        ]);

        return Parceiro::create($request->all());
    }

    /**
     * Mostrar parceiro específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Parceiro::find($id);
    }


    /**
     * Modifica o parceiro cadastrado.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parceiro = Parceiro::find($id);
        $parceiro->update($request->all());
        return $parceiro;
    }

    /**
     * Deleta o parceiro cadastrado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Parceiro::destroy($id);
    }
    public function serach($name)
    {
        return Parceiro::where('name', 'like', '%'.$name.'%')->get();
    }
}
